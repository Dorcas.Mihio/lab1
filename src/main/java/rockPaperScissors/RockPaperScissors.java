package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.Scanner;


public class RockPaperScissors {
	
	public static void main(String[] args) {
    
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    Double randNum= Math.random()*3;
    int choice=randNum.intValue();


   
   
    
    public void run() {
        while (true){
            System.out.println("Let's play round "+ roundCounter);
            String humanChoice="";
            while (true){
                humanChoice=readInput("Your choice (Rock/Paper/Scissors)?");
                if (humanChoice.equals("rock")||humanChoice.equals("paper")||humanChoice.equals("scissors")){
                    break;
                }else{
                    System.out.println("I do not understand " +humanChoice+". Could you try again?");
                }
                
            }
            Random  random =new Random();
            int random_num = random.nextInt(3);

            String computerChoice;
            if (random_num==0){
                computerChoice="rock";
            }else if(random_num==1){
                computerChoice="paper";
            }else {
                computerChoice="scissors";
            }

            if (humanChoice.equals(computerChoice)){
                System.out.println("Human chose " + humanChoice+", computer chose " +computerChoice+". It's a tie");
            } else if (humanChoice.equals("rock")){
                if (computerChoice.equals("paper")){
                    computerScore +=1;
                    System.out.println("Human chose " + humanChoice+", computer chose " +computerChoice+". Computer wins!");
                    
                }else{
                    ++humanScore;
                    System.out.println("Human chose " + humanChoice+", computer chose " +computerChoice+". Human wins!");
                }
            }
            else if (humanChoice.equals("paper")){
                if (computerChoice.equals("scissors")){
                    computerScore +=1;
                    System.out.println("Human chose " + humanChoice+", computer chose " +computerChoice+". Computer wins!");
                }else{
                    ++humanScore;
                    System.out.println("Human chose " + humanChoice+", computer chose " +computerChoice+". Human wins!");
                }

            }else if (humanChoice.equals("scissors")){
                if (computerChoice.equals("rock")){
                    computerScore +=1;
                    System.out.println("Human chose " + humanChoice+", computer chose " +computerChoice+". Computer wins!");
                }else{
                    ++humanScore;
                    System.out.println("Human chose " + humanChoice+", computer chose " +computerChoice+". Human wins!");
                }
            }
            System.out.println("Score: human "+humanScore+ ", computer "+computerScore);
            String continuePlaying= readInput("Do you wish to continue playing? (y/n)?");
            if (continuePlaying.equals("n")){
                System.out.println("Bye bye :)");
                break;
            }
        }
        ++roundCounter;
    }

    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next().toLowerCase();
        return userInput;
    }
    
}
